import type { IBookmark } from '../interfaces/IBookmark.ts';

export const bookmarks: IBookmark[] = [{
  href: 'https://example.com/',
  title: 'Example',
  description: 'Text large description',
  tags: [{
    id: 0,
    name: 'tag'
  }, {
    id: 0,
    name: 'tag'
  }]
}, {
  href: 'https://example.com/',
  title: 'Example',
  description: 'Text large description',
  tags: [{
    id: 0,
    name: 'tag'
  }, {
    id: 0,
    name: 'tag'
  }]
}, {
  href: 'https://example.com/',
  title: 'Example',
  description: 'Text large description',
  tags: [{
    id: 0,
    name: 'tag'
  }]
}, {
  href: 'https://example.com/',
  title: 'Example',
  description: 'Text large description',
  tags: [{
    id: 0,
    name: 'tag'
  }, {
    id: 0,
    name: 'tag'
  }]
}, {
  href: 'https://example.com/',
  title: 'Example',
  description: 'Text large description',
  tags: [{
    id: 0,
    name: 'tag'
  }]
}, {
  href: 'https://example.com/',
  title: 'Example',
  description: 'Text large description',
  tags: [{
    id: 0,
    name: 'tag'
  }]
}, {
  href: 'https://example.com/',
  title: 'Example',
  description: 'Text large description',
  tags: [{
    id: 0,
    name: 'tag'
  }, {
    id: 0,
    name: 'tag'
  }]
}];
