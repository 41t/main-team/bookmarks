import type { ITag } from './ITag.ts';

export interface IBookmark {
  href: string;
  title: string;
  description: string;
  tags: ITag[];
}